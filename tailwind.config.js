module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class' // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        body: ['Montserrat'],
        num: ['Anton'],
        jose: ['Josefin Sans'],
      },
      colors: {
        'black-20': 'rgba(0,0,0,0.2)',
        'black-80': 'rgba(0,0,0,0.8)',
        'white-20': 'rgba(255,255,255,0.20)',
        'white-50': 'rgba(255,255,255,0.50)'
      },
      backgroundImage: {
        'cold': "url('../images/cold-bg.jpg')",
        'warm': "url('../images/warm-bg.jpg')",
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
