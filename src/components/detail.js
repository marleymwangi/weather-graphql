import { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import { ReactComponent as Clouds } from '../images/clouds.svg';
import { ReactComponent as Thermo } from '../images/thermo.svg';
import { ReactComponent as Wind } from '../images/wind.svg';

export default function Detail({ name, values, resultsVariant }) {
  const [hovered, setHovered] = useState(false);
  const [icon, setIcon] = useState(null);
  const [info, setInfo] = useState(false);

  useEffect(() => {
    switch (name) {
      case 'clouds':
        setIcon(<Clouds/>)
        break;
      case 'temp':
        setIcon(<Thermo/>)
        break;
      case 'wind':
        setIcon(<Wind/>)
        break;

      default:
        setIcon(Clouds)
        break;
    }
  }, [name])

  return (
    <motion.div
      className={`${name} ${hovered && 'flair'}`}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      variants={resultsVariant}
    >
      {icon}
      <h5>{values[0].title}: <span>{values[0].val}</span></h5>
      <h5>{values[1].title}: <span>{values[1].val}</span></h5>
    </motion.div>
  )
}
