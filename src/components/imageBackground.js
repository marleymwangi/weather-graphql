import { useEffect } from 'react';
import { motion, useAnimation } from "framer-motion";
import cold from "../images/cold-sm.jpg";
import warm from "../images/warm-sm.jpg";

const transitionVariant = {
  hidden: {
    opacity: 0
  },
  visible: {
    opacity: 1,
    transition: {
      type: 'spring',
      duration: 3
    }
  }
}

export default function ImageBackground({ temp }) {
  const controls = useAnimation();

  useEffect(() => {
    if (temp > 23) {
      controls.start("visible");
    }else{
      controls.start("hidden")
    }
  }, [temp]);

  return (
    <div className="image-background">
      <img src={cold} alt="" />
      <motion.img
        variants={transitionVariant}
        initial="hidden"
        animate={controls}
        src={warm} alt="" />
      <div className="overlay" />
    </div>
  )
}
