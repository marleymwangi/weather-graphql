import { useState, useEffect } from 'react';
import { useLazyQuery } from '@apollo/client';
import { GET_WEATHER_QUERY } from '../graphql/Queries';
import { motion, AnimatePresence } from 'framer-motion';
import ReactAnimatedWeather from 'react-animated-weather';

import ImageBackground from './imageBackground';
import Detail from './detail';

const resultsContVariant = {
  hidden: {
    opacity: 0,
    height: 0
  },
  visible: {
    opacity: 1,
    height: 'auto',
    transition: {
      ease: "easeInOut",
      duration: 0.5,
      staggerChildren: .2,
      when: 'beforeChildren'
    }
  }
}

const resultsSecVariant = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
    transition: {
      staggerChildren: .1,
      when: 'beforeChildren'
    }
  }
}

const resultsVariant = {
  hidden: {
    opacity: 0,
    y: -20,
    scale: 0.97
  },
  visible: {
    opacity: 1,
    y: 0,
    scale: 1,
    transition: {
      type: 'spring',
      mass: 0.4,
      damping: 5,
    }
  }
}

const detailsSecVariant = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
    transition: {
      delayChildren: .2,
      staggerChildren: .2,
    }
  }
}

const Weather = () => {
  const [city, setCity] = useState("");
  const [cityWeather, setCityWeather] = useState(null);
  const [icon, setIcon] = useState({
    icon: 'CLOUDY',
    color: 'goldenrod',
    animate: true
  });

  useEffect(() => {
    if (city && city.length > 0) {
      getWeather();
    } else if (city.length < 1) {
      setCityWeather(null)
    }
  }, [city])

  const [getWeather, { loading, data, error }] = useLazyQuery(GET_WEATHER_QUERY, {
    variables: { name: city },
  })

  const kelvintoCelsius = (t) => {
    return Math.round(t - 273.15)
  }

  const dateBuilder = (dunix) => {
    let d = new Date(dunix * 1000)
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    return `${day} ${date} ${month} ${year}`
  }

  useEffect(() => {
    if (data?.getCityByName) setCityWeather(data.getCityByName);
    //data && console.log(data);
  }, [data])

  useEffect(() => {
    if (cityWeather !== undefined && cityWeather !== null) {
      switch (cityWeather.weather.summary.title) {
        case 'Clouds':
          if (cityWeather.weather.clouds.all > 50) {
            var hr = (new Date(cityWeather.weather.timestamp * 1000)).getHours();
            if (hr < 6 || hr > 20) {
              setIcon({
                icon: 'PARTLY_CLOUDY_DAY',
                color: 'goldenrod',
                animate: true
              });
            } else {
              setIcon({
                icon: 'PARTLY_CLOUDY_NIGHT',
                color: 'goldenrod',
                animate: true
              });
            }
          } else {
            setIcon({
              icon: 'CLOUDY',
              color: 'goldenrod',
              animate: true
            });
          }
          break;
        case 'Snow':
          setIcon({
            icon: 'SNOW',
            color: 'goldenrod',
            animate: true
          });
          break;
        case 'Drizzle':
          setIcon({
            icon: 'RAIN',
            color: 'goldenrod',
            animate: true
          });
          break;
        case 'Thunderstorm':
          setIcon({
            icon: 'RAIN',
            color: 'goldenrod',
            animate: true
          });
          break;
        case 'Rain':
          setIcon({
            icon: 'RAIN',
            color: 'goldenrod',
            animate: true
          });
          break;
        case 'Mist' || 'Smoke' || 'Haze' || 'Dust' || 'Fog' || 'Sand' || 'Ash' || 'Squall' || 'Tornado':
          setIcon({
            icon: 'FOG',
            color: 'goldenrod',
            animate: true
          });
          break;
        case 'Clear':
          var hr = (new Date(cityWeather.weather.timestamp * 1000)).getHours();
          if (hr < 6 || hr > 20) {
            setIcon({
              icon: 'CLEAR_NIGHT',
              color: 'goldenrod',
              animate: true
            });
          } else {
            setIcon({
              icon: 'CLEAR_DAY',
              color: 'goldenrod',
              animate: true
            });
          }
          break;

        default:
          break;
      }
    }
  }, [cityWeather])


  return (
    <div className="weather">
      <ImageBackground
        temp={cityWeather && kelvintoCelsius(cityWeather.weather.temperature.feelsLike)}
      />
      <main className="container">
        <input type='text'
          value={city}
          className='searchbar'
          placeholder='Enter city name'
          onChange={(e) => { setCity(e.target.value) }} />
        <AnimatePresence exitBeforeEntry>
          {
            cityWeather &&
            <motion.div
              className="result-container"
              variants={resultsContVariant}
              initial="hidden"
              animate="visible"
              exit="hidden"
            >
              <motion.div
                className="locationbox"
                variants={resultsSecVariant}
              >
                <motion.div
                  className="location"
                  variants={resultsVariant}
                >
                  {cityWeather.name}<span>, {cityWeather.country}</span></motion.div>
                <motion.div
                  className="date"
                  variants={resultsVariant}
                >
                  {dateBuilder(cityWeather.weather.timestamp)}</motion.div>
              </motion.div>
              <motion.div
                className="weatherbox"
                variants={resultsSecVariant}
              >
                <motion.div
                  className="temp"
                  variants={resultsVariant}
                >{kelvintoCelsius(cityWeather.weather.temperature.feelsLike)}°c</motion.div>
                <motion.div
                  className="divider"
                  variants={resultsVariant}
                />
                <motion.div
                  className="icon"
                  variants={resultsVariant}
                >
                  <ReactAnimatedWeather
                    icon={icon.icon}
                    color={icon.color}
                    animate={icon.animate}
                  />
                </motion.div>
                <motion.div className="summary" variants={resultsVariant}>
                  {cityWeather.weather.summary.title}<br />
                  <span>{cityWeather.weather.summary.description}</span>
                </motion.div>
              </motion.div>
              <motion.div
                className="details"
                variants={detailsSecVariant}
              >
                <Detail
                  name='clouds'
                  values={[
                    {
                      title: 'cover',
                      val: cityWeather.weather.clouds.all
                    },
                    {
                      title: 'humid',
                      val: cityWeather.weather.clouds.humidity
                    },
                  ]}
                  resultsVariant={resultsVariant}
                  cityWeather={cityWeather}
                />
                <Detail
                  name='temp'
                  values={[
                    {
                      title: 'feels',
                      val: kelvintoCelsius(cityWeather.weather.temperature.feelsLike)
                    },
                    {
                      title: 'actual',
                      val: kelvintoCelsius(cityWeather.weather.temperature.actual)
                    },
                  ]}
                  resultsVariant={resultsVariant}
                  cityWeather={cityWeather}
                />
                <Detail
                  name='wind'
                  values={[
                    {
                      title: 'speed',
                      val: cityWeather.weather.wind.speed
                    },
                    {
                      title: 'deg',
                      val: cityWeather.weather.wind.deg
                    },
                  ]}
                  resultsVariant={resultsVariant}
                  cityWeather={cityWeather}
                />
              </motion.div>
            </motion.div>
          }
        </AnimatePresence>
      </main>
    </div>
  );
}

export default Weather;