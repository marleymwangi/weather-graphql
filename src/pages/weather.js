import React from 'react';
import { useLazyQuery } from '@apollo/client';
import { GET_WEATHER_QUERY } from '../graphql/Queries';
import { motion, AnimatePresence } from 'framer-motion';

import Loader from '../components/Loader';


const Weather = () => {
  const [city, setCity] = React.useState("");

  React.useEffect(() => {
    if (city && city.length > 0) {
      getWeather();
    }
  }, [city])

  const [getWeather, { loading, data, error }] = useLazyQuery(GET_WEATHER_QUERY, {
    variables: { name: city },
  })

  const kelvintoCelsius = (t) => {
    return Math.round(t - 273.15)
  }

  const dateBuilder = (dunix) => {
    let d = new Date(dunix * 1000)
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    return `${day} ${date} ${month} ${year}`
  }

  if (data) {
    console.log(data);
  }

  return (
    <div className="app">
      <main>
        <motion.div
          className='searchbox'
          initial={{ opacity: 0, y: -100, scale: 0.5 }}
          animate={{ opacity: 1, y: 0, scale: 1 }}
          transition={{ type: 'spring' }}
        >
          <input type='text' className='searchbar' placeholder='Search...'
            onChange={(e) => { setCity(e.target.value) }} />
        </motion.div>
        {
          !data &&
          <motion.div
            className='weather-instr'
            initial={{ opacity: 0, scale: 0.5 }}
            animate={{ opacity: 1, scale: 1 }}
            transition={{ type: 'spring', delay: .2 }}
          >
            <p className=''>
              {loading ? 'Loading...' : 'Enter a city name'}
            </p>
          </motion.div>
        }

        <AnimatePresence exitBeforeEntry>
          {
            data && data.getCityByName &&
            <motion.div
              exit={{ opacity: 0, y: 30, scale: 0.5 }}
              initial={{ opacity: 0, y: 30, scale: 0.5 }}
              animate={{ opacity: 1, y: 0, scale: 1 }}
              transition={{ duration: .5 }}
            >
              <div className="locationbox">
                <div className="location">{data.getCityByName.name}, {data.getCityByName.country} </div>
                <div className="date">{dateBuilder(data.getCityByName.weather.timestamp)}</div>
              </div>
              <div className="weatherbox">
                <div className="temp">{kelvintoCelsius(data.getCityByName.weather.temperature.feelsLike)}°c</div>
                <div className="summary">{data.getCityByName.weather.summary.title}, <span>{data.getCityByName.weather.summary.description}</span></div>
              </div>
            </motion.div>
          }
        </AnimatePresence>
      </main>
    </div>
  );
}

export default Weather;