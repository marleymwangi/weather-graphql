import React from 'react';
import Weather from './components/weather';
import { ApolloClient, InMemoryCache, ApolloProvider, HttpLink } from '@apollo/client';
import { motion } from 'framer-motion';


function App() {

  const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: 'https://graphql-weather-api.herokuapp.com/'
  });

  return (
    <ApolloProvider client={client}>
      <section className='content'>
        <section className='weather-sec'>
          <Weather />
        </section>
        <section className='about-sec'>
          <div className='about'>
            <motion.p
              className='title'
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ type: 'spring', delay: .2 }}
            >
              React Weather app.
            </motion.p>
            <motion.p
              className='body'
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ type: 'spring', delay: .5 }}
            >
              Retrieve the current weather for any given city. Since this GraphQL API uses the free-tier of the Open Weather Map API, it is restricted to 60 calls/minute.
            </motion.p>
            <motion.p
              className='sub-title'
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ type: 'spring', delay: .6 }}
            >
              Technologies:
            </motion.p>
            <motion.p
              className='lang'
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ type: 'spring', delay: .7 }}
            >
              <span className='react'>React.</span> <span className='graphql'>Apollo GraphQl.</span>
            </motion.p>
          </div>
        </section>
      </section>
    </ApolloProvider>
  );
}

export default App;
